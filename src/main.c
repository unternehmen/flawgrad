#include <stdio.h>
#include "used.h"

/**
 * Run the program.
 *
 * @param argc  the number of command-line arguments
 * @param argv  the list of command-line arguments
 * @return      the exit status of the program
 */
int main(int argc, char *argv[]) {
        USED(argc);
        USED(argv);

        printf("Hello, world!\n");

        return 0;
}
