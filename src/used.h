/** @file used.h
 * Provides a macro for silencing errors about unused variables.
 */
#ifndef USED_H
#define USED_H

/**
 * Mark a variable as having been used.
 *
 * This allows unused variables to exist even when maximum compiler
 * warnings are enabled.
 *
 * @param x  the variable
 */
#define USED(x) ((void)(x))

#endif /* USED_H */
