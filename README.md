# Flawgrad
Flawgrad is a medieval fantasy life simulator.

## License
Flawgrad is released into the public domain via the Unlicense.  For more
information, see LICENSE.txt or visit <https://unlicense.org>.

